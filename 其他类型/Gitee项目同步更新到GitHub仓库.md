
### 前言：

个人开源项目由于好的迭代更新star人越来越多，有一些小伙伴说没有码云或者身在国外，他们比较喜欢用github，本人从svn摒弃后，一直用码云开发项目。所以对github的操作少之又少。突然某天有个小伙伴问我，你项目为什么不放在github，我说我访问太慢了，加上我对英文界面不是很熟练。但是经过很多小伙伴的强烈要求下，我把我自己的项目放到了github上。
我个人开发更新喜欢的是码云提交代码，所以码云的代码一直是最新的，对应github都是一直没更新，项目里面都是那种把码云里面的写好的代码然后复制到github这样的提交对于参与工作开源项目的人他的一些记录就会被你完全的覆盖，提交上去就是你的账号，无法查看别人的操作。经过我找资料找大神后，得出了自己的操作经验。完美提交到github，再也不用担心直接覆盖了。
撰写者:q87766867




## 导入Gitee项目到开发工具

如图：第一个为gitee项目第二个为github项目

![2个项目](https://images.gitee.com/uploads/images/2019/0529/122424_2adadf8b_123301.png "2个项目.png")

#### 例子操作如下：

##### 1.首先我们创建一个【码云导入到github仓库演示文件】，然后提交到码云上面！


![输入图片说明](https://images.gitee.com/uploads/images/2019/0529/123053_319ea422_123301.png "屏幕截图.png")

##### 2.提交到码云后如图
![输入图片说明](https://images.gitee.com/uploads/images/2019/0529/123118_4cd371d6_123301.png "屏幕截图.png")


##### 3.复制码云项目仓库地址
![输入图片说明](https://images.gitee.com/uploads/images/2019/0529/123426_220fedd1_123301.png "屏幕截图.png")




## Github同步更新操作

##### 1.把github仓库切换成码云仓库

![切换码云仓库](https://images.gitee.com/uploads/images/2019/0529/123529_5221fd90_123301.png "切换码云仓库.png")

##### 2.粘贴码云地址 输入账号密码 点击 Next
![粘贴码云地址](https://images.gitee.com/uploads/images/2019/0529/123643_b7740559_123301.png "粘贴码云地址.png")

##### 3.选择码云分支  然后点击 Add Spec  在勾选 force Update 在点击 Finish
![更新](https://images.gitee.com/uploads/images/2019/0529/123838_fe1a0682_123301.png "更新.png")

##### 4. 弹出提示更新到本地仓库  点击 ok按钮
![更新](https://images.gitee.com/uploads/images/2019/0529/123851_74c7e79a_123301.png "更新.png")

##### 5.合并本地仓库 Team》Merge
![合并操作](https://images.gitee.com/uploads/images/2019/0529/123932_70b4a188_123301.png "合并操作.png")

##### 6. 点击Merge 按钮  进行合并操作
![合并操作](https://images.gitee.com/uploads/images/2019/0529/123947_05f870bb_123301.png "合并操作.png")

##### 7.合并完成 项目里面看到合并后的文件

![合并完成](https://images.gitee.com/uploads/images/2019/0529/124520_adb8aeee_5032784.png "合并完成.png")

##### 8.我们需要把更新的文件提交到github上面

##### 1.提交更新
![提交到github](https://images.gitee.com/uploads/images/2019/0529/124731_03007c8b_5032784.png "提交到github.png")
##### 2.提交完成
![完成](https://images.gitee.com/uploads/images/2019/0529/124755_161c5357_5032784.png "完成.png")
##### 3.查看github仓库
![提交完成](https://images.gitee.com/uploads/images/2019/0529/124859_f9122c1f_5032784.png "屏幕截图.png")