# 使用Git优雅的备份和编辑博客

## #背景

作为一个大三的准码农，最近一直想找一个实习的工作，但是现实是残酷的：大部分简历投了之后石沉大海。

考虑到暑假没有找到实习，所以想着搭一个个人博客，把三年来的所学做一个总结，一来方便以后查阅，二来分享一下我的一些想法，也方便以后HR对我进行了解。

大致了解了一下搭建博客的几种方法后，最后我选择了一个以Django作为框架搭建的个人博客。搭建的步骤作者博客里已经写的已经非常详细了，推荐使用docker部署。

[作者博客主页](https://tendcode.com/)

[我的博客主页](<http://emm233.cn:8043/>)（临时链接）

修改之后整体我还是比较满意的，是我喜欢的简约风格。但也有美中不足的地方：

- 博客文章没有备份的机制，毕竟自建的博客不如专业网站稳定
- 后台编辑文章的界面实在是有点简陋，直接在线编辑不太友好

针对这两个问题经过我的一番思考，终于找到一个解决方法：使用git远程仓库同步博客。

## 1 原理

首先，博客的内容是存储在数据库中的：

![](IMG/1.png)

- 备份：查出所有文章信息，生成文件，接着使用Git把这些文件push到远程仓库
- 编辑：在本地clone远程仓库，在本地编辑保存，然后push到远程仓库。再由博客的服务器拉去远程仓库的文件，读取里面的信息插入数据库

## 2 实现

由于Git并没有信息转化为文件、操作数据库的功能，所以我使用SpringBoot自己写了一个程序来实现这些功能，但是它只适用于[izone](https://tendcode.com/)的博客，其他需要根据自己的情况进行修改

[项目地址](https://gitee.com/mythoflogic/git_blog)

## 3 部署

### 3.1 创建远程仓库

具体创建过程就不再叙述，可以是公有的也可以是私有的。

[我的测试仓库](https://gitee.com/mythoflogic/test)

### 3.2 下载程序

我已经将适用于[izone](https://tendcode.com/)的程序打包好了，可以直接下载使用, 其他博客要下载源码修改后打包：

![](IMG/2.png)

### 3.3 修改配置文件

下载解压后要修改SpringBoot的配置文件：

```properties
#修好程序运行的端口
server.port=8080
#通用数据源配置，修改为自己博客的数据库
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://ip:3306/izone?charset=utf8&useSSL=false
spring.datasource.username=root
spring.datasource.password=********

#开启驼峰转换
mybatis.configuration.map-underscore-to-camel-case=true

spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
spring.jpa.show-sql=true

#git远程仓库地址，修改第一步创建的Git远程仓库
config.remotePath=url
#git本地目录，可以不用修改
config.path=git/
#git账号，修改为自己的账号密码
config.user=123@qq.com
config.pwd=******
```

### 3.4 运行

运行即为SpringBoot的常规运行方式：

```shell
java -jar git_izone-1.0.0.jar
```

后台运行：

```shell
nohup java -jar git_izone-1.0.0.jar &
```

到这里所有的部署工作就完成了。

## 4 使用

### 4.1 备份博客

备份的时候，可以使用如下命令（根据自己的情况更换为自己的ip和端口），也可以在浏览器请求`ip:port/push/`

```shell
curl ip:port/push/
```

如果返回success就说明备份成功了：

![](IMG/3.png)

这时博客就被被分到Git的远程仓库了：

![](IMG/5.png)

![](IMG/4.png)

生成的文件会按照目录，存放在不同的目录下，大家可以去我[博客](http://emm233.cn:8043/)和[远程仓库](https://gitee.com/mythoflogic/test)看下效果：

其中md文件为博客的内容，json文件为图片、描述信息的配置文件：

![](IMG/6.png)

### 4.2 编辑博客

知道了如何备份博客，就理解为什么能优雅的编辑博客了，备份之后就可以像编辑本地文件一样编辑博客了！

首先要clone远程仓库，然后在已有的目录或者新建一个文件夹中新建一个与博客标题`同名`的文件夹，并新建`标题.md`h和`标题.json`：

![](IMG/8.png)



然后编写md和json文件，json文件按照如下格式：

```json
{
	"图片":"http://baiducdn.pig66.com/uploadfile/2017/0511/20170511074935785.jpg",
	"简介":"这里是描述信息"
}
```

![](IMG/9.png)

然后把修改提交，并push到远程仓库：

```shell
git add -A
git commot -m "提交的信息"
git push
```

最后通知服务器，更新数据库，同理要替换自己的ip和端口：

```shell
curl ip:port/clone/
```

返回success后(Springboot中时间有问题，所以是8小时前，这个以后会改正)：
![](IMG/10.png)

![](IMG/11.png)