# 用5G不？
发布三方库到jcenter百度下就会有很多教程，为什么我还要写这个呢？因为太麻烦！！！太费时！！！
第一次用bintray发布时，好像用了几个小时，经常连接不上。

现在给大家推荐一个快捷发布的方法：gitee + jitpack

最开始用jitpack时，只支持github，操作起来也慢，不过也还好吧。现在支持gitee了，强烈推荐大家用gitee，相对与github，这就像用5G和2G上网的区别，还有人用2G吗？

# 发布 [AdapterX](https://gitee.com/DonaldDu/AdapterX)
首先上传代码到gitee并创建tag，然后复制项目地址到jitpack，最后在jitpack上点击几下就搞定了！
![](https://images.gitee.com/uploads/images/2019/0620/155832_1854f909_97873.png)

![](https://images.gitee.com/uploads/images/2019/0620/160807_9dcb6a40_97873.png)

######  还可以像下图一样在MD中显示三方库的最新版本信息
![](https://images.gitee.com/uploads/images/2019/0620/161223_2fd3bb34_97873.png)

![](https://images.gitee.com/uploads/images/2019/0620/161732_9da89227_97873.png)