## 缘起
之前说了那么多关于git和码云相关的事，一直都没给大伙讲解这个码云究竟是个啥玩意儿。

今天就给大伙说说如何通过git和码云搭建属于自己的代码库。

## 码云
码云(gitee)是开源中国社区团队推出的基于Git的快速的、免费的、稳定的在线代码托管平台,不限制私有库和公有库数量.

`github`大伙总该听说过的吧，码云就是中国版的`github`，如果有兴趣，可以一起了解一下，毕竟有时候`github`抽起风来，啥都搞不来。所以还是国内的环境好，中国的`github`还是很适用于大部分国人的，虽然里面的内容不多，但是相较于`github`的国外服务器，码云在国内的速度那是杠杠的，而且，最最关键的是，私有项目免费！私有项目免费！私有项目免费！！！

>官网地址：https://gitee.com/

>第一步，注册

>第二步，登录

登录之前先去邮箱验证下吧

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img01.png)


>创建项目

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img02.png)

首先映入眼帘的是创建项目和完善资料，如果不太喜欢自己的头像或者其他信息，可以先完善资料，我们先来创建我们的第一个项目哈~

点击创建项目

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img03.png)

给项目起个名字吧，就叫test吧。

语言的话先选择PHP吧，添加.gitignore文件，开源许可证可参考下图进行选择

### 关于许可证

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img04.png)

我们选择Apache许可证

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img05.png)

你可以将你的项目设置成私有的，免费的，免费的，免费的，真是棒~

使用README初始化这个项目，这个文件可以让你写一些类似于用户须知的文档

## git管理

在开始git clone项目之前，你们首先要在你们的码云里面设置好可以clone代码的keygen。

我们在创建完项目的时候，会进入到这个页面

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img06.png)

看到右上角的管理没有，点击它

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img07.png)

点击添加公钥，如果你想让你的项目可以进行写操作，点击那个黄色的添加个人公钥。

### 如何生成公钥
```bash
ssh-keygen -t rsa -C "xxxxx@xxxxx.com"
```

一路Enter下去哈~别输入内容就行啦~别问为什么，因为你会被坑的~

```bash
cat ~/.ssh/id_rsa.pub
```

将屏幕打印出来的内容复制到刚才的个人公钥里面去，就是下面图中红色部分

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img08.png)

点击确定，添加完成，下面就到你本地去clone你的项目啦~

### clone项目
回到项目的首页，看到右上角的没，点击它，弹出一个框，选择ssh方式clone代码

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img09.png)

复制这行代码，到你本地，找个文件夹，开始clone吧

```bash
cd ~/gitosproject
git clone git@git.oschina.net:ifengye/test.git
```

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img10.png)

看到这行，说明远程仓库的代码已经被clone下来啦！

```bash
cd test
```
下面开始撸代码吧，啪啪啪啪啪啪啪啪，好了，撸完了，我们来提交我们的代码哈~

### 提交项目
一个好习惯，不管这个项目多少人参与，push之前先pull一下，看看有没有merge的代码，有merge解决merge，没有的话直接下面的命令

```bash 
git pull origin master
git add .
git commit -m 'init'
git push origin master
```

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img11.png)

OK，下面我们去看下代码是不是被添加上来了~

![](http://psvg5cjmd.bkt.clouddn.com/img/mayun/img12.png)

看到这个界面，我就放心了，因为我之前在gitee上搞过事，就用的之前的账户提交的代码，如果你有很多员工需要提交代码，那么你可以在设置里面添加他们的账号进来，给他们读写代码的权限。总之，码云还是很给力的。开心的撸代码吧，以后到哪都可以撸代码啦，好开心~~~